<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactosMensajes extends Model
{
    protected $table = 'contactosMensajes';

    protected $fillable = ['contactos_id', 'asunto', 'mensaje', 'sn_visto'];
    protected $primaryKey = 'id';
    // protected $hidden = ['updated_at', 'created_at'];

    public function scopeContacto($query, $contactos_id = '') {
        if (!empty($contactos_id) and !is_null($contactos_id)) {
    	    return $query->where('contactos_id', $contactos_id);
        }
    }

    public function scopeDtFechaInicio($query, $created_at = '') {
        if (!empty($created_at and !is_null($created_at))) {
            return $query->where('contactosMensajes.created_at', '>=', $created_at.' 00:00:00');
        }
    }

    public function scopeDtFechaFin($query, $created_at = '') {
        if (!empty($created_at and !is_null($created_at))) {
            return $query->where('contactosMensajes.created_at', '<=', $created_at.' 23:59:59');
        }
    }
}

<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class Exportar implements FromCollection, WithHeadings
{
    //use Exportable;
    /**
    * No cambiar el nombre de las funciones  son propias de las clases
    */

    protected $datos;
    protected $nombres;

    public function __construct($datos = null, array $nombres )
    {
        $this->datos = $datos;
        $this->nombres =  $nombres;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->datos; 
    } 
    public function headings(): array
    {
        return $this->nombres;
    } 

    /*public function query()
    {
        return $this->datos; 
    }*/

}

<?php

namespace App\Http\Controllers;

use App\AdminPropiedades;
use App\PropiedadesImagenes;

use Illuminate\Http\Request;

class PDFController extends Controller
{
    public function PDF($nu_propiedad) {
		$propiedades = AdminPropiedades::select(
			'admin_propiedades.nu_propiedad',
			'admin_propiedades.ln_nombre',
			'admin_propiedades.ln_numero_casa',
			'admin_propiedades.nu_precio',
			'admin_propiedades.ln_moneda',
			'admin_propiedades.nu_banio',
			'admin_propiedades.nu_cochera',
			'admin_propiedades.nu_cochera',
			'admin_propiedades.nu_dormitorio',
			'admin_propiedades.nu_metros_vivienda',
			'admin_propiedades.nu_metros_terreno',
			'admin_propiedades.nu_tipo_inmueble',
			'admin_propiedades.nu_tipo_operacion',
			'admin_propiedades.ln_direccion',
			'admin_propiedades.ln_url_mapa',
			'admin_propiedades.ln_detalles',
			'tipo_inmuebles.ln_tipo_inmueble',
			'tipo_operaciones.ln_tipo_operacion'
			)
			->join('tipo_inmuebles','tipo_inmuebles.nu_tipo_inmueble', 'admin_propiedades.nu_tipo_inmueble')
			->join('tipo_operaciones','tipo_operaciones.nu_tipo_operacion', 'admin_propiedades.nu_tipo_operacion')
			->PropiedadesImagen()
			->where('admin_propiedades.nu_propiedad',$nu_propiedad)
			->where('admin_propiedades.nu_activo','1')
			->get();

		$imagenes = PropiedadesImagenes::where('nu_propiedad', $nu_propiedad)->where('nu_principal', '0')->limit(3)->get();

		$nombrePropiedad = "TuCasaSma";
		if (strlen($propiedades) > 0) {
			$nombrePropiedad = $propiedades[0]->ln_nombre;
		}

		$rutaImg = "https://www.tucasasma.com";

		$Principal = simplexml_load_string(\Storage::disk('local')->get('Principal.xml'));
		$telefonoPrincipal = "";
		$emailPrincipal = "";
		if (isset($Principal->Telefono->Principal)) {
			$telefonoPrincipal = $Principal->Telefono->Principal;
		}
		if (isset($Principal->Email->Principal)) {
			$emailPrincipal = $Principal->Email->Principal;
		}

    	$pdf = \PDF::loadView('pdfPropiedades', ['propiedades' => $propiedades, 'imagenes' => $imagenes, "nombrePropiedad" => $nombrePropiedad, "rutaImg" => $rutaImg, "telefonoPrincipal" => $telefonoPrincipal, "emailPrincipal" => $emailPrincipal]);
    	return $pdf->stream($nombrePropiedad.'.pdf');
    }
}

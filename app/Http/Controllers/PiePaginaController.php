<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PiePaginaController extends Controller {

    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        if(request()->ajax()){
            $dataRequest = request()->all();

            $xml = new \DomDocument('1.0', 'UTF-8'); //Se crea el docuemnto

            $raiz = $xml->createElement('Contacto');
            $raiz = $xml->appendChild($raiz);

            // Telefonos
            $nodo_First = $xml->createElement('Titulos');
            $nodo_First = $raiz->appendChild($nodo_First);

            $nodo_Second = $xml->createElement('Principal', $dataRequest['txtPrinicipal']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            $nodo_Second = $xml->createElement('Descripcion', $dataRequest['txtDescripcion']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            // Email
            $nodo_First = $xml->createElement('Derechos');
            $nodo_First = $raiz->appendChild($nodo_First);

            $nodo_Second = $xml->createElement('Texto', $dataRequest['txtDerechos']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            //Se eliminan espacios en blanco
            $xml->preserveWhiteSpace = false;

            //Se ingresa formato de salida
            $xml->formatOutput = true;

            //Se instancia el objeto
            $xml_string =$xml->saveXML();

            //Y se guarda en el nombre del archivo 'achivo.xml', y el obejto nstanciado
            \Storage::disk('local')->put('PiePagina.xml',$xml_string);

            return response()->json(["intState"=>1,"strMensaje"=>"Información guardada correctamente.","contenido"=>$xml_string],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","contenido"=>""],400) ;
        }
    }

    public function verXml() {
        $xml = \Storage::disk('local')->get('PiePagina.xml');
        return response($xml)->withHeaders([ 'Content-Type' => 'text/xml']);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create()
    {

    }

    /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store()
    {

    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {

    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id)
    {

    }

    /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update($id)
    {

    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {

    }
  
}

?>
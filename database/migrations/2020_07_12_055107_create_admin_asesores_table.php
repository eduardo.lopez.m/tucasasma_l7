<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminAsesoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_asesores', function (Blueprint $table) {
            $table->bigIncrements('nu_asesor');
            $table->string('ln_nombre');
            $table->string('ln_puesto')->nullable();
            $table->string('ln_descripcion')->nullable();
            $table->string('ln_link_facebook')->nullable();
            $table->string('ln_link_twitter')->nullable();
            $table->string('ln_link_linked')->nullable();
            $table->string('ln_link_whattsapp')->nullable();
            $table->smallInteger('nu_cod_pais')->nullable();
            $table->string('ln_telefono');
            $table->string('ln_correo');
            $table->string('ln_url_imagen')->nullable();
            $table->smallInteger('nu_activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_asesores');
    }
}

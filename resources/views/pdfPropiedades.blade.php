<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>{{$nombrePropiedad}}</title>
	</head>
	<body>
		<div align="center">
			<img align="center" src="{{$rutaImg}}/img/logo_tucasasma1_300.png" width="130px" height="90px" />
		</div>
		@foreach($propiedades as $propiedad)
			<div>
				<h1 align="center" style="margin-bottom:-20px;">{{$propiedad->ln_nombre}}</h1>
				<h4 align="center">{{$propiedad->ln_numero_casa}} {{$propiedad->ln_direccion}}</h4>
				<h3 align="center">$ {{number_format($propiedad->nu_precio,2,'.',',')}} {{$propiedad->ln_moneda}} </h3>
				<h5 align="center" style="margin-bottom:-20px;">{{$propiedad->nu_dormitorio}} Habitaciones, {{$propiedad->nu_banio}} Baños, {{$propiedad->nu_metros_terreno}} M2</h5>
				<h5 align="center"> Tipo de Inmueble: {{$propiedad->ln_tipo_inmueble}},  Tipo Operación: {{$propiedad->ln_tipo_operacion}}, M2 de Vivienda: {{$propiedad->nu_metros_vivienda}}</h5>
				<h4>Mas Información</h4>
				<p>{{$propiedad->ln_detalles}}</p>
				<h4>Información de Contacto</h4>
				<p>Teléfono: {{$telefonoPrincipal}}, Correo: {{$emailPrincipal}}, Dirección: San Miguel de Allende, Guanajuato, México</p>
				<h4>Imágenes y Características Ilustrativas</h4>
			</div>
			<div align="center">
				<img style="margin-top: 3%;" src="{{$rutaImg}}/{{$propiedad->ln_url_imagen}}" width="70%" height="315px" />
			</div>

			@foreach($imagenes as $imagen)
				<div align="center" style="margin-bottom:2%;">
					<img src="{{$rutaImg}}/{{$imagen->ln_url_imagen}}" width="70%" height="315px" /> 
				</div>
			@endforeach
	        
		@endforeach
	</body>
</html>
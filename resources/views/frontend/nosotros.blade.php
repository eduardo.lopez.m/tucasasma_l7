@extends('layoutsFront.app')
@section('content')
    <!-- Quines Somos -->  
    @include('frontend.nosotrosEncabezado')  

    <div class="site-section site-section-mission-vision ">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6" data-aos="fade-up" data-aos-delay="200">
                <div class="team-member box-mision">
                    <div class="text">
                    <h2 class="mb-3">Visión</h2>
                    <p class="text-justify text-light-gray" id="idNosotrosVision"></p>
                    </div>
                </div>
                </div>

                <div class="col-md-6 col-lg-6" data-aos="fade-up" data-aos-delay="200">
                <div class="team-member box-mision">
                    <div class="text">
                    <h2 class="mb-3">Misión</h2>
                    <p class="text-justify text-light-gray" id="idNosotrosMision"></p>
                    </div>
                </div>
                </div>            
            </div>
        </div>
    </div>

    <!-- Banner -->
    <div class="section-banner">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-4 border-color" data-aos="fade-up" data-aos-delay="100">
            <a href="{{asset('Propiedades/1/-1/-1')}}" class="service text-center">
              <span class="icon flaticon-house"></span>
              <h2 class="service-heading">Residencial</h2>
              <!-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iure qui natus perspiciatis ex odio molestia.</p>
              <p><span class="read-more">Read More</span></p> -->
            </a>
          </div>
          <div class="col-md-6 col-lg-4 border-color" data-aos="fade-up" data-aos-delay="100">
            <a href="{{asset('Propiedades/2/-1/-1')}}" class="service text-center">
              <span class="icon flaticon-flat"></span>
              <h2 class="service-heading">Departamentos</h2>
              <!-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iure qui natus perspiciatis ex odio molestia.</p>
              <p><span class="read-more">Read More</span></p> -->
            </a>
          </div>
          <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="100">
            <a href="{{asset('Propiedades/3/-1/-1')}}" class="service text-center">
              <span class="icon flaticon-location"></span>
              <h2 class="service-heading">Terrenos</h2>
              <!-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iure qui natus perspiciatis ex odio molestia.</p>
              <p><span class="read-more">Read More</span></p> -->
            </a>
          </div>
        </div>
      </div>
    </div>

    <!-- Seleccionar opcion del menu -->
    <script src="{{ asset('ajax/menuFront.js') }}"></script>
@endsection
@extends('layoutsFront.app')
@section('content')

    <!-- Contacto -->
    <div class="site-section" >
      <div class="container mt-6">
        <div class="row mb-5 justify-content-center">
          <div class="col-md-7">
            <div class="site-section-title text-center">
              <h2 class="lblTituloPrincial" id="idContactoPrincipal"></h2>
              <p id="idContactoDescripcion"></p>
            </div>
          </div>
        </div>
        
        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-md-6 col-lg-4" >
            <a href="#" class="service text-center">
              <span class="iconservices icon-map-marker"></span>
              <h2 class="service-heading">Dirección</h2>
              <p class="mb-4 text-center"><a href="https://goo.gl/maps/asV85WqFteLSwRoi8" target="_blank"> San Miguel de Allende, Guanajuato, México</a></p>
            </a>
          </div>
          <div class="col-md-6 col-lg-4">
            <a href="#" class="service text-center">
              <span class="iconservices icon-phone_in_talk"></span>
              <h2 class="service-heading">Teléfono</h2>
              <p class="mb-4 text-center"><a href="tel:" id="idContactoTelefono"></a></p>
            </a>
          </div>
          <div class="col-md-6 col-lg-4">
            <a href="#" class="service text-center">
              <span class="iconservices icon-envelope"></span>
              <h2 class="service-heading">Correo</h2>
              <p class="mb-4 text-center"><a href="mailto:" id="idContactoEmail"></a></p>
            </a>
          </div>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-md-12">
            <form role="form" class="p-5 bg-white border" id="frmContacto">
              {{ csrf_field() }}
              <div class="row form-group">
                <div class="col-md-4 mb-3 mb-md-0">
                  <label class="font-weight-bold" for="txtName">Nombre Completo <span id="txtNameError" style="color: red;" class="hide">Requerido</span></label>
                  <input type="text" id="txtName" name="txtName" class="form-control" placeholder="Nombre Completo">
                </div>
                <div class="col-md-4">
                  <label class="font-weight-bold" for="txtTelefono">Teléfono <span id="txtTelefonoError" style="color: red;" class="hide">Requerido</span></label></label>
                  <input type="text" id="txtTelefono" name="txtTelefono" class="form-control" placeholder="Teléfono">
                </div>
                <div class="col-md-4">
                  <label class="font-weight-bold" for="txtEmail">Correo <span id="txtEmailError" style="color: red;" class="hide">Requerido</span></label>
                  <input type="email" id="txtEmail" name="txtEmail" class="form-control" placeholder="Correo">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-md-12">
                  <label class="font-weight-bold" for="txtAsunto">Asunto <span id="txtAsuntoError" style="color: red;" class="hide">Requerido</span></label>
                  <input type="text" id="txtAsunto" name="txtAsunto" class="form-control" placeholder="Agregar Asunto">
                </div>
              </div>
              <div class="row form-group">
                <div class="col-md-12">
                  <label class="font-weight-bold" for="txtMessage">Mensaje <span id="txtMessageError" style="color: red;" class="hide">Requerido</span></label> 
                  <textarea name="message" id="txtMessage" name="txtMessage" cols="30" rows="3" class="form-control" placeholder="Mensaje"></textarea>
                </div>
              </div>
              <div class="row form-group">
                <div class="col-md-12">
                  <input type="button" value="Enviar Mensaje" id="btnEnviarMensaje" class="btn btn-primary  py-2 px-4 rounded-0">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Mapa -->
    <div class="site-mapa">
        <div class="row">
            <div class="col-md-12" id="idContactoMaps">
                <!-- <iframe class="mapacontacto" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29815.44861221049!2d-100.76221207078252!3d20.91509244586992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842b51af9871bc0f%3A0x653101031d07b0a4!2sSan%20Miguel%20de%20Allende%2C%20Gto.!5e0!3m2!1ses!2smx!4v1593666309948!5m2!1ses!2smx" width="450" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->
            </div>
        </div>
    </div>

    <!-- Banner -->
    <div class="section-banner bg-light">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-4 border-color" data-aos="fade-up" data-aos-delay="100">
            <a href="{{asset('Propiedades/1/-1/-1')}}" class="service text-center">
              <span class="icon flaticon-house"></span>
              <h2 class="service-heading">Residencial</h2>
              <!-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iure qui natus perspiciatis ex odio molestia.</p>
              <p><span class="read-more">Read More</span></p> -->
            </a>
          </div>
          <div class="col-md-6 col-lg-4 border-color" data-aos="fade-up" data-aos-delay="100">
            <a href="{{asset('Propiedades/2/-1/-1')}}" class="service text-center">
              <span class="icon flaticon-flat"></span>
              <h2 class="service-heading">Departamentos</h2>
              <!-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iure qui natus perspiciatis ex odio molestia.</p>
              <p><span class="read-more">Read More</span></p> -->
            </a>
          </div>
          <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="100">
            <a href="{{asset('Propiedades/3/-1/-1')}}" class="service text-center">
              <span class="icon flaticon-location"></span>
              <h2 class="service-heading">Terrenos</h2>
              <!-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt iure qui natus perspiciatis ex odio molestia.</p>
              <p><span class="read-more">Read More</span></p> -->
            </a>
          </div>
        </div>
      </div>
    </div>

    <!-- Seleccionar opcion del menu -->
    <script src="{{ asset('ajax/menuFront.js') }}"></script>
@endsection

<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element" align="center"> 
                    <span>
                        <img alt="image" class="img-circle" src="{{ asset('img/logo_tucasasma1_icono.png') }}" />
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> 
                            <span class="block m-t-xs"> 
                                <strong class="font-bold">{{ Auth::user()->name }}</strong>
                            </span> 
                            <span class="text-muted text-xs block">{{ Auth::user()->email }} <b class="caret"></b></span> 
                        </span> 
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <!-- <li><a href="profile.html">Profile</a></li>
                        <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="mailbox.html">Mailbox</a></li>
                        <li class="divider"></li> -->
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Cerrar Sesión</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    <img alt="image" class="img-circle" src="{{ asset('img/logo_tucasasma1_icono.png') }}" />
                </div>
            </li>
            <li id="mdlAdminPrincipal">
                <a href="/AdminPrincipal"><i class="fa fa-cog"></i> <span class="nav-label">Principal</span> <!-- <span class="fa arrow"></span> --></a>
            </li>
            <li id="mdlAdminPropiedades">
                <a href="/AdminPropiedades"><i class="fa fa-home"></i> <span class="nav-label">Propiedades</span> <!-- <span class="fa arrow"></span> --></a>
            </li>
            <li id = "mdlCarruselSMA">
                <a href="/AdminCarruselSMA"><i class="fa fa-indent"></i> <span class="nav-label">Carrusel SMA</span> <!-- <span class="fa arrow"></span> --></a>
            </li>
            <li id="mdlAdminAsesores">
                <a href="/AdminAsesores"><i class="fa fa-group"></i> <span class="nav-label">Asesores</span> <!-- <span class="fa arrow"></span> --></a>
            </li>
            <li id="mdlAdminNosotros">
                <a href="/AdminNosotros"><i class="fa fa-book"></i> <span class="nav-label">Nosotros</span> <!-- <span class="fa arrow"></span> --></a>
            </li>
            <li id="mdlAdminContacto">
                <a href="/AdminContacto"><i class="fa fa-map-marker"></i> <span class="nav-label">Contacto</span> <!-- <span class="fa arrow"></span> --></a>
            </li>
            <li id="mdlAdminPiePagina">
                <a href="/AdminPiePagina"><i class="fa fa-exclamation-circle"></i> <span class="nav-label">Pie Página</span> <!-- <span class="fa arrow"></span> --></a>
            </li>
            <li id="mdlAdminContactosMsg">
                <a href="/AdminContactoMsg"><i class="fa fa-envelope"></i> <span class="nav-label">Contactos (Mensajes)</span> <!-- <span class="fa arrow"></span> --></a>
            </li>
            <li class="" id="mdlUsuarios">
                <a href="/abcUsuarios" style="font-size: 14px;"><i class="fa fa-user"></i> <span class="nav-label">Usuarios</span> <!-- <span class="fa arrow"></span> --></a>
            </li>
        </ul>
    </div>
</nav>

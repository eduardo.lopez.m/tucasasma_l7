$(document).ready(function () {

    $('#mdlCarruselSMA').addClass('active');
    
    /* Configuracion de tabla */
	$('.dataTables-example').DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Publicación SMA'},
            {extend: 'pdf', title: 'Publicación SMA'},
            {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        columnDefs: [
            { className: 'text-center', targets: [2,3,4] }
        ],
    });

    fnOntenerRegistros();

    $("#btnAgregarNuevo").click(function(){
        $("#ln_titulo").removeClass("has-error");
        $("#ln_url_imagen").removeClass("has-error");
        $("#ln_descripcion").removeClass("has-error");
        
        var nombreCampo = {
            'ln_titulo':'titulo',
            'ln_url_imagen':'imagen',
            'ln_descripcion':'descripción'
        };

        var msg = fnValidarFormulario('frmFormulario', nombreCampo);
        if(msg){ return; }
        
        fnAgregarRegistro();
    });

    $("#btnModificarRegistro").click(function(){
        fnModificarRegistro($("#nu_publicidad").val());
    });

    $("#btnBuscar").click(function(){
        fnOntenerRegistros();
    });

    $('#mdlFormulario').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var title = "";

        $("#btnAgregarNuevo").css("display","none");
        $("#btnModificarRegistro").css("display","none");

        if(button.data('nu_publicidad')>0){
            title="Modificar Publicación";
            $("#btnModificarRegistro").css("display","");
            fnEditarRegistro(button.data('nu_publicidad'));
        }else{
            title="Nueva Publicación";
            $("#btnAgregarNuevo").css("display","");
            $("#nu_publicidad").val('');
            $("#ln_titulo").val('');
            $("#ln_descripcion").val('');
            $("#ln_url_imagen").val('');
            $("#imgTemporal").attr('src', "");
            $("#nu_activo").prop('checked',true).iCheck('update');
        }
        
        modal.find('.modal-title').text('' + title)
        
    });
});

$(document).on("ifChanged",".chkEstatus",function() {
    var nu_activo = 0;
    
    if($(this).prop('checked')){
        nu_activo = 1;
    }

    fnModificarEstatus($(this).data('nu_publicidad'), nu_activo);
});

$(document).on("click",".btnEliminar",function() {
    var nu_publicidad = $(this).data('nu_publicidad');
    swal({
        title: "¿Desea eliminar el registro?",
        text: "No podrás recuperar la información!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false
    }, function () {
        fnEliminarRegistro(nu_publicidad);
    });
    
});

function fnEditarRegistro(nu_publicidad){
    var url = 'CarruselSMA/'+nu_publicidad;
    var formData = new FormData();

    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('GET',url,formData);
    
    if(infoData.intState){
        if(infoData.publicidad !== null){
            $.each(infoData.publicidad,function(index, el) {
                $("#nu_publicidad").val(el.nu_publicidad);
                $("#ln_titulo").val(el.ln_titulo);
                $("#ln_descripcion").val(el.ln_descripcion);
                $("#ln_url_imagen").val('');
                $("#imgTemporal").attr('src', el.ln_url_imagen);
                $("#nu_activo").prop('checked',false).iCheck('update');
                if(el.nu_activo=="1"){
                    $("#nu_activo").prop('checked',true).iCheck('update');
                }
            });
        }
    }
}

function fnAgregarRegistro(){
    var url = 'CarruselSMA';
    var formData = new FormData(document.getElementById("frmFormulario"));
    
    formData.append("_method", 'POST');

    var infoData = fnAjaxSelect('POST',url,formData);
    
    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto!", text: infoData.strMensaje, type: "success"});
        fnOntenerRegistros();
    }
}

function fnEliminarRegistro(nu_publicidad){
    var formData = new FormData();
    var url = 'CarruselSMA/'+nu_publicidad;

    formData.append("_method", 'DELETE');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        swal("Eliminado!", infoData.strMensaje, "success");
        fnOntenerRegistros();
    }else{
        swal("En uso!", infoData.strMensaje, "error");
    }
}

function fnModificarRegistro(nu_publicidad){
    var url = 'CarruselSMA/'+nu_publicidad;
    var formData = new FormData(document.getElementById("frmFormulario"));

    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto", text: infoData.strMensaje, type: "success"});
        fnOntenerRegistros();
    }
}

function fnModificarEstatus(nu_publicidad,nu_activo){
    var url = 'CarruselSMA/'+nu_publicidad;
    var formData = new FormData();
    
    formData.append("nu_activo", nu_activo);
    formData.append("_token", $('input[name="_token"]').attr('value'));
    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        fnMensajeInformativoSuccess(infoData.strMensaje);
    }
}

function fnOntenerRegistros(){
    var formData = new FormData();
    formData.append("ln_titulo", $('#txtTitulo').val());
    formData.append("nu_activo", "");
    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));
    var infoData = fnAjaxSelect('POST','CarruselSMA',formData);

    if(infoData.intState){
        if(infoData.publicidad !== null){
            var estatus="";

            var btnEliminar="";
            var btnModificar="";

            var chkEstatus="";
            var strImagen="";

            if($('#tblRegistros').DataTable().data().length > 0){
                $('#tblRegistros').DataTable().clear();
                $('#tblRegistros').DataTable().draw();
            }

            console.log(infoData.publicidad);
            $.each(infoData.publicidad,function(index, el) {
                
                chkEstatus="checked";
                
                if(el.nu_activo == 0){
                    chkEstatus="";
                }

                estatus = '<input type="checkbox" name="nu_activo" class="i-checks chkEstatus" data-nu_publicidad="'+el.nu_publicidad+'" style="position: absolute; opacity: 0;" '+chkEstatus+'>';

                btnModificar='<button class="btn btn-primary btn-xs btnModificar" type="button" data-toggle="modal" data-target="#mdlFormulario" data-nu_publicidad="'+el.nu_publicidad+'"><i class="fa fa-edit"></i></button>';
                btnEliminar='<button class="btn btn-danger btn-xs btnEliminar" type="button" data-nu_publicidad="'+el.nu_publicidad+'"><i class="fa fa-trash"></i></button>';

                $('#tblRegistros').dataTable().fnAddData( 
                    [
                        el.ln_titulo,
                        el.ln_descripcion, 
                        "<img src='"+el.ln_url_imagen+"' style='width: 50%;height: auto;'></img>",                         
                        estatus,
                        btnModificar +" "+btnEliminar
                    ]
                );
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        }
    }
}


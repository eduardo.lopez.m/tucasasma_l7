$(document).ready(function () {

    $('#mdlAdminAsesores').addClass('active');
    
    /* Configuracion de tabla */
	$('.dataTables-example').DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Publicación SMA'},
            {extend: 'pdf', title: 'Publicación SMA'},
            {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        columnDefs: [
            { className: 'text-center', targets: [3,4,5,6,7] }
        ],
    });

    fnOntenerRegistros();

    $("#btnAgregarNuevo").click(function(){
        $("#ln_nombre").removeClass("has-error");
        $("#ln_puesto").removeClass("has-error");
        $("#ln_correo").removeClass("has-error");
        $("#ln_telefono").removeClass("has-error");
        
        var nombreCampo = {
            'ln_nombre':'Nombre del asesorr',
            'ln_puesto':'Puesto del asesor',
            'ln_correo':'Correo',
            'ln_telefono':'Teléfono'
        };

        var msg = fnValidarFormulario('frmFormulario', nombreCampo);
        if(msg){ return; }
        fnAgregarRegistro();
    });

    $("#btnModificarRegistro").click(function(){
        fnModificarRegistro($("#nu_asesor").val());
    });

    $("#btnBuscar").click(function(){
        fnOntenerRegistros();
    });

    $('#mdlFormulario').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var title = "";

        $("#btnAgregarNuevo").css("display","none");
        $("#btnModificarRegistro").css("display","none");

        if(button.data('nu_asesor')>0){
            title="Modificar Asesor";
            $("#btnModificarRegistro").css("display","");
            fnEditarRegistro(button.data('nu_asesor'));
        }else{
            title="Nuevo Asesor";
            $("#btnAgregarNuevo").css("display","");
            $("#nu_asesor").val('');
            $("#ln_nombre").val('');
            $("#ln_puesto").val('');
            $("#ln_correo").val('');
            $("#ln_telefono").val('');
            $("#ln_link_facebook").val('');
            $("#ln_link_twitter").val('');
            $("#ln_link_linked").val('');
            $("#ln_link_whattsapp").val('');
            $("#nu_cod_pais").val('');
            $("#ln_descripcion").val('');
            $("#ln_url_imagen").val('');
            $("#imgTemporal").attr('src', "");
            $("#nu_activo").prop('checked',true).iCheck('update');
        }
        
        modal.find('.modal-title').text('' + title)
        
    });
});

$(document).on("ifChanged",".chkEstatus",function() {
    var nu_activo = 0;
    
    if($(this).prop('checked')){
        nu_activo = 1;
    }

    fnModificarEstatus($(this).data('nu_asesor'), nu_activo);
});

$(document).on("click",".btnEliminar",function() {
    var nu_asesor = $(this).data('nu_asesor');
    swal({
        title: "¿Desea eliminar el registro?",
        text: "No podrás recuperar la información!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false
    }, function () {
        fnEliminarRegistro(nu_asesor);
    });
    
});

function fnEditarRegistro(nu_asesor){
    var url = 'urlAdminAsesores/'+nu_asesor;
    var formData = new FormData();

    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('GET',url,formData);
    
    if(infoData.intState){
        if(infoData.asesor !== null){
            $.each(infoData.asesor,function(index, el) {
                $("#nu_asesor").val(el.nu_asesor);
                $("#ln_nombre").val(el.ln_nombre);
                $("#ln_puesto").val(el.ln_puesto);
                $("#ln_descripcion").val(el.ln_descripcion);
                $("#ln_url_imagen").val('');
                $("#ln_correo").val(el.ln_correo);
                $("#ln_telefono").val(el.ln_telefono);
                $("#ln_link_facebook").val(el.ln_link_facebook);
                $("#ln_link_twitter").val(el.ln_link_twitter);
                $("#ln_link_linked").val(el.ln_link_linked);
                $("#ln_link_whattsapp").val(el.ln_link_whattsapp);
                $("#nu_cod_pais").val(el.nu_cod_pais);
                $("#imgTemporal").attr('src', el.ln_url_imagen);
                $("#nu_activo").prop('checked',false).iCheck('update');
                if(el.nu_activo=="1"){
                    $("#nu_activo").prop('checked',true).iCheck('update');
                }
            });
        }
    }
}

function fnAgregarRegistro(){
    var url = 'urlAdminAsesores';
    var formData = new FormData(document.getElementById("frmFormulario"));
    
    formData.append("_method", 'POST');

    var infoData = fnAjaxSelect('POST',url,formData);
    
    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto!", text: infoData.strMensaje, type: "success"});
        fnOntenerRegistros();
    }
}

function fnEliminarRegistro(nu_asesor){
    var formData = new FormData();
    var url = 'urlAdminAsesores/'+nu_asesor;

    formData.append("_method", 'DELETE');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        swal("Eliminado!", infoData.strMensaje, "success");
        fnOntenerRegistros();
    }else{
        swal("En uso!", infoData.strMensaje, "error");
    }
}

function fnModificarRegistro(nu_asesor){
    var url = 'urlAdminAsesores/'+nu_asesor;
    var formData = new FormData(document.getElementById("frmFormulario"));

    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto", text: infoData.strMensaje, type: "success"});
        fnOntenerRegistros();
    }
}

function fnModificarEstatus(nu_asesor,nu_activo){
    var url = 'urlAdminAsesores/'+nu_asesor;
    var formData = new FormData();
    
    formData.append("nu_activo", nu_activo);
    formData.append("_token", $('input[name="_token"]').attr('value'));
    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        fnMensajeInformativoSuccess(infoData.strMensaje);
    }
}

function fnOntenerRegistros(){
    var formData = new FormData();
    formData.append("ln_nombre", $('#txtNombre').val());
    formData.append("nu_activo", '');
    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));
    var infoData = fnAjaxSelect('POST','urlAdminAsesores',formData);

    if(infoData.intState){
        if(infoData.asesores !== null){
            var estatus="";

            var btnEliminar="";
            var btnModificar="";

            var chkEstatus="";
            var strImagen="";

            if($('#tblRegistros').DataTable().data().length > 0){
                $('#tblRegistros').DataTable().clear();
                $('#tblRegistros').DataTable().draw();
            }

            $.each(infoData.asesores,function(index, el) {
                
                chkEstatus="checked";
                
                if(el.nu_activo == 0){
                    chkEstatus="";
                }

                estatus = '<input type="checkbox" name="nu_activo" class="i-checks chkEstatus" data-nu_asesor="'+el.nu_asesor+'" style="position: absolute; opacity: 0;" '+chkEstatus+'>';

                btnModificar='<button class="btn btn-primary btn-xs btnModificar" type="button" data-toggle="modal" data-target="#mdlFormulario" data-nu_asesor="'+el.nu_asesor+'"><i class="fa fa-edit"></i></button>';
                btnEliminar='<button class="btn btn-danger btn-xs btnEliminar" type="button" data-nu_asesor="'+el.nu_asesor+'"><i class="fa fa-trash"></i></button>';

                $('#tblRegistros').dataTable().fnAddData( 
                    [
                        el.ln_nombre,
                        el.ln_puesto,
                        el.ln_descripcion, 
                        el.nu_cod_pais+el.ln_telefono, 
                        el.ln_correo, 
                        "<img src='"+el.ln_url_imagen+"' style='width: 50%;height: auto;'></img>",                         
                        estatus,
                        btnModificar +" "+btnEliminar
                    ]
                );
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        }
    }
}


$(document).ready(function () {

    $('#mdlAdminPropiedades').addClass('active');
    
    /* Configuracion de tabla */
	$('.dataTables-example').DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Publicación SMA'},
            {extend: 'pdf', title: 'Publicación SMA'},
            {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        columnDefs: [
            { className: 'text-right', targets: [2] },
            { className: 'text-center', targets: [3,4,5,6] }
        ],
    });

    fnOntenerRegistros();

    $("#btnAgregarNuevo").click(function(){
        // $("#ln_nombre").removeClass("has-error");
        // $("#nu_precio").removeClass("has-error");
        // $("#ln_moneda").removeClass("has-error");
        // $("#nu_tipo_inmueble").removeClass("has-error");
        // $("#nu_tipo_operacion").removeClass("has-error");
        
        // var nombreCampo = {
        //     'ln_nombre':'Nombre de la propiedad'
        //     'nu_precio':'Precio de la propiedad',
        //     'ln_moneda':'Moneda',
        //     'nu_tipo_inmueble':'Tipo inmueble',
        //     'nu_tipo_operacion':'Tipo operacion'
        // };

        // var msg = fnValidarFormulario('frmFormulario', nombreCampo);
        // if(msg){ return; }

        fnAgregarRegistro();
    });

    $("#btnModificarRegistro").click(function(){
        fnModificarRegistro($("#nu_propiedad").val());
    });

    $("#btnBuscar").click(function(){
        fnOntenerRegistros();
    });

    $('#btnNuevo').click(function() {
        $('#divFormulario').css('display','block');
        $('#divFiltros').css('display','none');
        
        var title="Nueva Propiedad";
        $("#btnAgregarNuevo").css("display","");
        $("#btnModificarRegistro").css("display","none");
        fnLimpiarCamposForm('frmFormulario');
        $("#divImagenes").empty();
        $("#nu_activo").prop('checked',true).iCheck('update');
        $("#nu_principal").prop('checked',true).iCheck('update');

        
        $('#lblFormulario').text(title);
        
    });

    $("#btnRegresar").click(function(){
        $('#divFormulario').css('display','none');
        $('#divFiltros').css('display','');
    });

});

$(document).on("ifChanged",".chkEstatus",function() {
    var nu_activo = 0;
    
    if($(this).prop('checked')){
        nu_activo = 1;
    }

    fnModificarEstatus($(this).data('nu_propiedad'), nu_activo);
});

$(document).on("ifChanged",".chkCarrusel",function() {
    var nu_principal = 0;
    
    if($(this).prop('checked')){
        nu_principal = 1;
    }

    fnModificarEstatus($(this).data('nu_propiedad'), nu_principal,1);
});

$(document).on("click",".btnEliminar",function() {
    var nu_propiedad = $(this).data('nu_propiedad');
    swal({
        title: "¿Desea eliminar el registro?",
        text: "No podrás recuperar la información!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false
    }, function () {
        fnEliminarRegistro(nu_propiedad);
    });
    
});

$(document).on("click",".btnModificar",function() {
    $("#btnModificarRegistro").css("display","");
    $("#btnAgregarNuevo").css("display","none");
    $('#divFormulario').css('display','block');
    $('#divFiltros').css('display','none');
    $("#divImagenes").empty();

    var title="Modificar Propiedad";

    fnLimpiarCamposForm('frmFormulario');
    fnEditarRegistro($(this).data('nu_propiedad'));

    $('#lblFormulario').text(title);
});

$(document).on("click",".btnImgPrincipal",function() {
    fnModificarImgPrincipal($(this).data('nu_imagen'));
    $(".btnImgPrincipal").removeClass("btn-success");
    $(".btnImgPrincipal").addClass("btn-default");
    $(".btnImgPrincipal").text("Principal");
    $(this).removeClass("btn-default");
    $(this).addClass("btn-success");
    $(this).css("color","white");
    $(this).text("Imagen Principal");
});

$(document).on("click",".btnEliminarImagen",function() {
    var nu_imagen = $(this).data('nu_imagen');
    swal({
        title: "¿Desea eliminar el registro?",
        text: "No podrás recuperar la información!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false
    }, function () {
        fnEliminarImagen(nu_imagen);
        var div = $(this).parent("div"); 
        console.log(div);
        div.remove();
        div.empty();
        $('#imagen'+nu_imagen).remove();
    });
});

function fnModificarImgPrincipal(nu_imagen){
    var url = 'PropiedadesImagenes/'+nu_imagen;
    var formData = new FormData();

    formData.append("nu_imagen", nu_imagen);
    formData.append("_token", $('input[name="_token"]').attr('value'));
    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        fnMensajeInformativoSuccess(infoData.strMensaje);
    }
}

function fnEliminarImagen(nu_imagen){
    var formData = new FormData();
    var url = 'PropiedadesImagenes/'+nu_imagen;

    formData.append("_method", 'DELETE');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        swal("Eliminado!", infoData.strMensaje, "success");
    }else{
        swal("En uso!", infoData.strMensaje, "error");
    }
}

function fnLimpiarCamposForm(componente){
    $('#'+componente+' input[type="text"],input[type="file"], textarea').each(
        function(index){  
            var input = $(this);
            input.val('');
        }
    );

    $('#'+componente+' select').each(
        function(index){  
            var input = $(this);
            input.val('');
            input.multiselect('rebuild');
        }
    );

    $("#divImagenes").empty();
}

function fnEditarRegistro(nu_propiedad){
    var url = 'urlAdminPropiedades/'+nu_propiedad;
    var formData = new FormData();

    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('GET',url,formData);
    
    if(infoData.intState){
        if(infoData.propiedades !== null){
            $.each(infoData.propiedades,function(index, el) {
                $("#nu_propiedad").val(el.nu_propiedad);
                $("#ln_nombre").val(el.ln_nombre);
                $("#nu_precio").val(el.nu_precio);
                $("#ln_moneda").val(el.ln_moneda);
                $("#ln_numero_casa").val(el.ln_numero_casa);
                $("#nu_dormitorio").val(el.nu_dormitorio);
                $("#nu_banio").val(el.nu_banio);
                $("#nu_cochera").val(el.nu_cochera);
                $("#nu_tipo_inmueble").val(el.nu_tipo_inmueble);
                $("#nu_tipo_operacion").val(el.nu_tipo_operacion);
                $("#nu_metros_terreno").val(el.nu_metros_terreno);
                $("#nu_metros_vivienda").val(el.nu_metros_vivienda);
                $("#ln_direccion").val(el.ln_direccion);
                $("#ln_correo").val(el.ln_correo);
                $("#ln_telefono").val(el.ln_telefono);
                $("#ln_detalles").val(el.ln_detalles);
                $("#ln_url_mapa").val(el.ln_url_mapa);

                $('#frmFormulario .selectFormatoGeneral').multiselect('rebuild');

                $("#nu_activo").prop('checked',false).iCheck('update');
                if(el.nu_activo=="1"){
                    $("#nu_activo").prop('checked',true).iCheck('update');
                }

                $("#nu_principal").prop('checked',false).iCheck('update');
                if(el.nu_principal=="1"){
                    $("#nu_principal").prop('checked',true).iCheck('update');
                }
            });
        }

        if(infoData.imagenes !== null){
            var rowImagenes="";
            $.each(infoData.imagenes,function(index, el) {
                rowImagenes+="<div id='imagen"+el.nu_imagen+"' class='col-md-3 col-lg-2'>";
                rowImagenes+='<button type="button" class="btn btn-danger btn-sm btnEliminarImagen" data-nu_imagen="'+el.nu_imagen+'"><i class="fa fa-trash"></i></button>';
                rowImagenes+="<img src='"+el.ln_url_imagen+"' class='imgPropiedades'>";
                if(el.nu_principal == 1){
                    rowImagenes+="<button type='button' class='btn btn-success btn-sm btnImgPrincipal btn-block' data-nu_imagen='"+el.nu_imagen+"'>Imagen Principal</button>";
                }else{
                    rowImagenes+="<button type='button' class='btn btn-default btn-sm btnImgPrincipal btn-block' data-nu_imagen='"+el.nu_imagen+"'>Principal</button>";
                }
                
                rowImagenes+="</div>";
            });

            $("#divImagenes").empty();
            $("#divImagenes").append(rowImagenes);
        }
    }
}

function fnAgregarRegistro(){
    var url = 'urlAdminPropiedades';
    var formData = new FormData(document.getElementById("frmFormulario"));
    
    formData.append("_method", 'POST');

    var infoData = fnAjaxSelect('POST',url,formData);
    
    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto!", text: infoData.strMensaje, type: "success"});
        fnOntenerRegistros();
    }
}

function fnEliminarRegistro(nu_propiedad){
    var formData = new FormData();
    var url = 'urlAdminPropiedades/'+nu_propiedad;

    formData.append("_method", 'DELETE');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        swal("Eliminado!", infoData.strMensaje, "success");
        fnOntenerRegistros();
    }else{
        swal("En uso!", infoData.strMensaje, "error");
    }
}

function fnModificarRegistro(nu_propiedad){
    var url = 'urlAdminPropiedades/'+nu_propiedad;
    var formData = new FormData(document.getElementById("frmFormulario"));

    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        $('#mdlFormulario').modal('hide');
        swal({title: "Correcto", text: infoData.strMensaje, type: "success"});
        fnOntenerRegistros();
        fnLimpiarCamposForm('frmFormulario');
        $('#divFormulario').css('display','none');
        $('#divFiltros').css('display','');
    }
}

function fnModificarEstatus(nu_propiedad,nu_activo, chk=0){
    var url = 'urlAdminPropiedades/'+nu_propiedad;
    var formData = new FormData();
    
    if(chk ==1){
        formData.append("nu_principal", nu_activo);
    }else{
        formData.append("nu_activo", nu_activo);
    }
    formData.append("_token", $('input[name="_token"]').attr('value'));
    formData.append("_method", 'PUT');
    
    var infoData = fnAjaxSelect('POST',url,formData);

    if(infoData.intState){
        fnMensajeInformativoSuccess(infoData.strMensaje);
    }
}

function fnOntenerRegistros(){
    var formData = new FormData();
    formData.append("ln_nombre", $('#txtNombre').val());
    formData.append("ln_moneda", $('#cmbMoneda').val());
    formData.append("_method", 'GET');
    formData.append("_token", $('input[name="_token"]').attr('value'));
    var infoData = fnAjaxSelect('POST','urlAdminPropiedades',formData);

    if(infoData.intState){
        if(infoData.propiedades !== null){
            var estatus="";
            var carrusel="";

            var btnEliminar="";
            var btnModificar="";

            var chkEstatus="";
            var chkCarrusel="";
            var strImagen="";

            if($('#tblRegistros').DataTable().data().length > 0){
                $('#tblRegistros').DataTable().clear();
                $('#tblRegistros').DataTable().draw();
            }

            $.each(infoData.propiedades,function(index, el) {
                
                chkEstatus="checked";
                chkCarrusel="checked";
                
                if(el.nu_activo == 0){
                    chkEstatus="";
                }

                if(el.nu_principal == 0){
                    chkCarrusel="";
                }

                estatus = '<input type="checkbox" class="i-checks chkEstatus" data-nu_propiedad="'+el.nu_propiedad+'" style="position: absolute; opacity: 0;" '+chkEstatus+'>';
                carrusel = '<input type="checkbox" class="i-checks chkCarrusel" data-nu_propiedad="'+el.nu_propiedad+'" style="position: absolute; opacity: 0;" '+chkCarrusel+'>';
                btnModificar='<button class="btn btn-primary btn-xs btnModificar" type="button" data-nu_propiedad="'+el.nu_propiedad+'"><i class="fa fa-edit"></i></button>';
                btnEliminar='<button class="btn btn-danger btn-xs btnEliminar" type="button" data-nu_propiedad="'+el.nu_propiedad+'"><i class="fa fa-trash"></i></button>';

                $('#tblRegistros').dataTable().fnAddData( 
                    [
                        el.ln_nombre,
                        el.ln_numero_casa+' '+el.ln_direccion, 
                        "$ "+number_format(el.nu_precio,2,'.',','),                         
                        el.ln_moneda,                         
                        carrusel,
                        estatus,
                        btnModificar +" "+btnEliminar
                    ]
                );
            });

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        }
    }
}


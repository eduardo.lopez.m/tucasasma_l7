$(document).ready(function () {

    $('#mdlAdminNosotros').addClass('active');

    $("#btnGuardar").click(function(){
        fnGuardarContacto();
    });

    fnObtenerInfoNosotros(1, 'txtPrinicipal', 'txtSecundario', 'txtDescripcion', 'txtMision', 'txtVision', 'txtLinkImagen');

});

/**
 * Función para guardar la información de la configuración
 * @return {[type]} [description]
 */
function fnGuardarContacto() {
    var formData = new FormData(document.getElementById("frmFormulario"));
    formData.append("_method", 'POST');

    var nombreCampo = {
        'txtPrinicipal':'título principal',
        'txtSecundario':'título cecundario',
        'txtDescripcion':'descripción',
        'txtMision':'misión',
        'txtVision':'visión'
    };

    var msg = fnValidarFormulario('frmFormulario', nombreCampo);
    if(msg){ return; }

    var infoData = fnAjaxSelect('POST','nosotrosAjax',formData);
    if(infoData.intState){
        swal({title: "Información!", text: infoData.strMensaje, type: "success"});
    } else {
        swal({title: "Información!", text: 'Ocurrio un problema, verifica con el administrador.', type: "warning"});
    }
}
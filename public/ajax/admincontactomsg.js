$(document).ready(function () {

    $('#mdlAdminContactosMsg').addClass('active');
    
    /* Configuracion de tabla */
	$('#tblRegistros').DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Contactos (Mensajes)'},
            {extend: 'pdf', title: 'Contactos (Mensajes)'},
            {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1,2,3,4] }
        ],
    });

    $('#tblRegistrosMsg').DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Contactos (Mensajes)'},
            {extend: 'pdf', title: 'Contactos (Mensajes)'},
            {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1,2] }
        ],
        order: [[ 0, "desc" ]]
    });

    // fnOntenerContactos();

    $("#btnBuscar").click(function(){
        fnOntenerContactos();
    });

    $('#mdlFormulario').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);

        modal.find('.modal-title').text(''+button.data('name'));
        
        fnOntenerContactosMsg(button.data('contacto'));
    });
});

/**
 * Función para obtener la información de los contactos
 * @return {[type]} [description]
 */
function fnOntenerContactos(){
    var formData = new FormData();
    formData.append("txtName", $('#txtName').val());
    formData.append("txtTelefono", $('#txtTelefono').val());
    formData.append("txtEmail", $('#txtEmail').val());
    formData.append("dtpFechaInicio", $('#dtpFechaInicio').val());
    formData.append("dtpFechaFin", $('#dtpFechaFin').val());
    formData.append("_method", 'POST');
    formData.append("_token", $('input[name="_token"]').attr('value'));
    var infoData = fnAjaxSelect('POST','contactosPanel',formData);

    if(infoData.intState){
        if(infoData.contenido !== null){
            var btnMensajes="";

            if($('#tblRegistros').DataTable().data().length > 0){
                $('#tblRegistros').DataTable().clear();
                $('#tblRegistros').DataTable().draw();
            }

            $.each(infoData.contenido,function(index, el) {
                btnMensajes='<button class="btn btn-primary btn-xs" type="button" data-toggle="modal" data-target="#mdlFormulario" data-contacto="'+el.id+'" data-name="'+el.name+'"><i class="fa fa-envelope"></i></button>';
                $('#tblRegistros').dataTable().fnAddData( 
                    [
                        el.name,
                        el.tel,
                        el.email, 
                        el.created_at,
                        btnMensajes
                    ]
                );
            });
        }
    }
}

/**
 * Función para obtener los mensajes del contacto
 * @param  {[type]} contacto [description]
 * @return {[type]}           [description]
 */
function fnOntenerContactosMsg(contacto){
    var formData = new FormData();
    formData.append("contacto", contacto);
    formData.append("dtpFechaInicio", $('#dtpFechaInicio').val());
    formData.append("dtpFechaFin", $('#dtpFechaFin').val());
    formData.append("_method", 'POST');
    formData.append("_token", $('input[name="_token"]').attr('value'));

    var infoData = fnAjaxSelect('POST','contactosMsg',formData);
    
    if(infoData.intState){
        if(infoData.contenido !== null){
            if($('#tblRegistrosMsg').DataTable().data().length > 0){
                $('#tblRegistrosMsg').DataTable().clear();
                $('#tblRegistrosMsg').DataTable().draw();
            }

            $.each(infoData.contenido,function(index, el) {
                $('#tblRegistrosMsg').dataTable().fnAddData( 
                    [
                        el.created_at,
                        el.asunto,
                        el.mensaje
                    ]
                );
            });
        }
    }
}